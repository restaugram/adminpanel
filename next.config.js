const withPlugins = require("next-compose-plugins");

const withLess = require("next-with-less");

const plugins = [
  [withLess, {
    lessLoaderOptions: {
    },
  }],
];
const nextConfig = {
  reactStrictMode: true,
  compiler: {
    // ssr and displayName are configured by default
    styledComponents: true,
  },
}

module.exports = withPlugins(plugins, nextConfig);