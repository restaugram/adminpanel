import Cookies from 'js-cookie';
import { USER_FIRST_PART, USER_SECOND_PART, USER_THIRD_PART } from "data/static/storageNames";
import {LOGIN} from "data/static/routes";

export const getUser = () => {
	return Cookies.get(USER_FIRST_PART) + "." + Cookies.get(USER_SECOND_PART) + "." + Cookies.get(USER_THIRD_PART)
}

export const isUserAuth = () => {
	return getUser() !== 'undefined.undefined.undefined';
}

export const logOut = () => {
	Cookies.remove(USER_FIRST_PART);
	Cookies.remove(USER_SECOND_PART);
	Cookies.remove(USER_THIRD_PART);
	window.location.href=LOGIN
}
