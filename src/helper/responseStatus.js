import {
	toastServerError,
	toastUnauthorizedError,
	toastAuthError,
	toastUserNotFound,
	toastAuthorizationSuccess,
	toastCategoryAddSlugRepeat,
	toastCategoryParentIdNotFound,
	toastAddCategorySuccess,
	toastDeleteCategorySuccess,
	toastEditCategorySuccess,
	toastCategorySlugError,
	toastAddAdminsSuccess,
	toastDeleteAdminsSuccess,
	toastEditAdminsSuccess,
	toastDeleteUserSuccess,
	toastDeleteBusinessSuccess,
} from './toastify';
export const authorizationStatusValidation = (status) => {
	if (status === 200) toastAuthorizationSuccess();
	else if (status === 500) toastServerError();
	else if (status === 404) toastUserNotFound();
	else if (status === 401) toastAuthError();
};


export const moderatorAddStatusValidation = (status) => {
	if (status === 200) toastAddAdminsSuccess();
	else if (status === 500) toastCategoryAddSlugRepeat();
	else if (status === 404) toastUserNotFound();
	else if (status === 400) toastAdmin();
	else if (status === 401) toastAuthError();
}

export const moderatorRemoveStatusValidation = (status) => {
	if (status === 204) toastDeleteAdminsSuccess();
	else if (status === 500) toastServerError();
	else if (status === 404) toastUserNotFound();
	else if (status === 401) toastAuthError();
}

export const moderatorEditStatusValidation = (status) => {
	if (status === 204) toastEditAdminsSuccess();
	else if (status === 500) toastServerError();
	else if (status === 404) toastUserNotFound();
	else if (status === 401) toastAuthError();
}

export const businessRemoveStatusValidation = (status) => {
	if (status === 204) toastDeleteBusinessSuccess();
	else if (status === 500) toastServerError();
	else if (status === 404) toastCategoryParentIdNotFound();
	else if (status === 401) toastAuthError();
}


export const userRemoveStatusValidation = (status) => {
	if (status === 204) toastDeleteUserSuccess();
	else if (status === 500) toastServerError();
	else if (status === 404) toastCategoryParentIdNotFound();
	else if (status === 401) toastAuthError();
}


export const categoryAddStatusValidation = (status) => {
	if (status === 200) toastAddCategorySuccess();
	else if (status === 500) toastCategoryAddSlugRepeat();
	else if (status === 404) toastCategoryParentIdNotFound();
	else if (status === 400) toastCategorySlugError();
	else if (status === 401) toastAuthError();
}

export const categoryRemoveStatusValidation = (status) => {
	if (status === 204) toastDeleteCategorySuccess();
	else if (status === 500) toastServerError();
	else if (status === 404) toastCategoryParentIdNotFound();
	else if (status === 401) toastAuthError();
}


export const categoryEditStatusValidation = (status) => {
	if (status === 204) toastEditCategorySuccess();
	else if (status === 500) toastServerError();
	else if (status === 404) toastCategoryParentIdNotFound();
	else if (status === 401) toastAuthError();
}
