import { notification } from 'antd';
import exp from "constants";



export const toastAuthorizationSuccess = () => {
	notification.success({
		message: 'Вы успешно вошли в систему',
		duration: 5,
		placement: 'bottomRight'
	});
}


export const toastAddAdminsSuccess = () => {
	notification.success({
		message: `Вы успешно добавили модератора`,
		duration: 5,
		placement: 'bottomRight'
	});
}

export const toastEditAdminsSuccess = () => {
	notification.success({
		message: `Вы успешно изменили модератора`,
		duration: 5,
		placement: 'bottomRight'
	});
}

export const toastDeleteAdminsSuccess = () => {
	notification.success({
		message: `Вы успешно удалили модератора`,
		duration: 5,
		placement: 'bottomRight'
	});
}

export const toastDeleteBusinessSuccess = () => {
	notification.success({
		message: `Вы успешно удалили бизнес`,
		duration: 5,
		placement: 'bottomRight'
	});
}


export const toastDeleteUserSuccess = () => {
	notification.success({
		message: `Вы успешно удалили модератора`,
		duration: 5,
		placement: 'bottomRight'
	});
}

export const toastAddCategorySuccess = () => {
	notification.success({
		message: `Вы успешно создали категорию`,
		duration: 5,
		placement: 'bottomRight'
	});
}

export const toastEditCategorySuccess = () => {
	notification.success({
		message: `Вы успешно изменили категорию`,
		duration: 5,
		placement: 'bottomRight'
	});
}

export const toastDeleteCategorySuccess = () => {
	notification.success({
		message: `Вы успешно удалили категорию`,
		duration: 5,
		placement: 'bottomRight'
	});
}

//Errors Toast
//Status 500 Iternal Server Error
export const toastServerError = () => {
	notification.error({
		message: 'Ошибка 500',
		description:
			'На данный момент на стороне сервера ошибка, пожалуйста повторите попытку позже',
		duration: 5,
		placement: 'bottomRight'
	});
};

//Status 404
export const toastUserNotFound = () => {
	notification.error({
		message: 'Ошибка 404',
		description: 'Данный пользователь не найден',
		duration: 5,
		placement: 'bottomRight'
	});
};

export const toastCategoryParentIdNotFound = () => {
	notification.error({
		message: 'Ошибка 404',
		description: 'Данный parentId не был найден',
		duration: 5,
		placement: 'bottomRight'
	});
};

//Status 401
export const toastAuthError = () => {
	notification.error({
		message: 'Ошибка 401',
		description: 'Вы неверно ввели логин или пароль',
		duration: 5,
		placement: 'bottomRight'
	});
};

export const toastUnauthorizedError = () => {
	notification.error({
		message: 'Вы не аутентифицированы',
		description: 'Для выполнения этих действий вам надо зайти в систему',
		duration: 5,
		placement: 'bottomRight'
	});
};


export const toastCategoryAddSlugRepeat = () => {
	notification.error({
		message: 'Slug который вы ввели повторяется',
		description: 'Для успешного добавление напишите другой slug',
		duration: 5,
		placement: 'bottomRight'
	});
}

//400

export const toastAdminOccupedLoginError = () => {
	notification.error({
		message: 'Данный логин уже зарегистрирован ',
		description: 'Поменяйте логин регистраций',
		duration: 5,
		placement: 'bottomRight'
	});
}

export const toastCategorySlugError = () => {
	notification.error({
		message: 'Slug который вы ввели не подходит под правил',
		description: 'Slug должен быть с маленькими буквами и без кирилицы',
		duration: 5,
		placement: 'bottomRight'
	});
}
