export interface ICategory{
    id: string,
    imageUrl: string,
    parentId: string,
    children: any,
    slug: string,
    name: string,
}
