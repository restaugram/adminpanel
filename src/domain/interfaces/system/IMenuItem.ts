import {ReactElement} from "react";

export interface IMenuItem{
    id: string,
    icon: ReactElement
    children: any,
    slug: string,
    name: string,
}
