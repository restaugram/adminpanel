export interface IBusiness{
    id: string,
    businessName: string,
    businessEmail: string,
    description: string,
    registrationDate: string,
    workDayStartTime: string,
    workDayEndTime: string
    categoryId: string
}
