export interface IAdmin{
    id: string,
    login: string,
    role: string
}