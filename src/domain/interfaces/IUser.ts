export interface IUser{
    id: string,
    email: string,
    fullName: string,
    phone: string,
    rating: number,
    birthDate: string
}
