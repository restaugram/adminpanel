import { Input } from 'antd';
import styled from "styled-components";
import { EyeInvisibleOutlined, EyeTwoTone } from '@ant-design/icons';

export const CustomInput = styled(Input)`
  background-color: #FCFDFE;
  border: 1px solid #F0F1F7;
  color: #4B506D;
  padding: 10px 20px;
  border-radius: 10px;
`

export const CustomInputPassword = styled(Input.Password)`
  background-color: #FCFDFE;
  border: 1px solid #F0F1F7;
  color: #4B506D;
  padding: 10px 20px;
  border-radius: 10px;
`

export const InputComponentWrapper = styled.div`
  display: flex;
  flex-direction: column;
`

export const InputLabel = styled.h4`
  font-weight: bold;
  margin-bottom: 5px;
  color: #9FA2B4
`

interface IInput{
    label: string,
    placeholder: string,
    type: string,
}

const InputComponent = ({label, placeholder, type} : IInput) => {
    return(
        <InputComponentWrapper>
            <InputLabel>{label}</InputLabel>
            {type === "password" ? (
                <CustomInputPassword iconRender={(visible) =>
                    visible ? (
                        <EyeTwoTone />
                    ) : (
                        <EyeInvisibleOutlined />
                    )
                } placeholder={placeholder} type={type}/>
            ) : (
                <CustomInput placeholder={placeholder} type={type}/>
            )}
        </InputComponentWrapper>
    )
}

export default InputComponent;