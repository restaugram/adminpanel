import React, { useState } from "react";
import {
  Table,
  Form,
  Typography,
  Tooltip,
  Button,
} from "antd";
import { EditFilled, DeleteFilled } from "@ant-design/icons";
import styled from "styled-components";

import {IAdmin} from "domain/interfaces/IAdmin";
import OperationModal from "./OperationModal";
import {
  moderatorAddStatusValidation,
  moderatorEditStatusValidation,
  moderatorRemoveStatusValidation
} from "helper/responseStatus";
import {adminsService} from "data/API";

const TableActionWrapper = styled.div`
  display: inline-flex;
  justify-content: space-around;
  font-size: 20px;
  width: 100%;
`;

interface IAdminTable {
  admins: Array<IAdmin>;
}

const AdminTable = ({ admins }: IAdminTable) => {
  const [form] = Form.useForm();
  const [isVisible, setIsVisible] = useState(false);
  const [editId, setEditId] = useState<any>(null);

  const [data, setData] = useState(admins);

  const onFinishAdd = async (values: any) => {
    await adminsService
      .adminAddApi(
        values.login,
        values.password
      )
      .then(async (responseAdd) => {
        moderatorAddStatusValidation(responseAdd.status)
        const response = await adminsService.refreshAdmins();
        setData(response.data);
        setIsVisible(false);
        form.setFieldsValue({ login: "", password: ""});
      });
  };

  const onFinishEdit = async (values: any) => {
    console.log(values)
    await adminsService
      .editAdminApi(
        editId,
        values.login,
        values.password
      )
      .then(async (responseEdit) => {
        moderatorEditStatusValidation(responseEdit)
        const response = await adminsService.refreshAdmins();
        setData(response.data);
        setIsVisible(false);
        form.setFieldsValue({ login: "", password: ""});
      });
  };


  const add = () => {
    setEditId(null);
    setIsVisible(true);
  };

  const edit = (record: Partial<IAdmin>) => {
    form.setFieldsValue({
      login: record.login,
      password: ""
    });
    setEditId(record.id);
    setIsVisible(true);
  };

  const deleteRecord = async (record: Partial<IAdmin>) => {
    await adminsService.deleteAdminApi(record.id).then(async (responseDelete) => {
      console.log(responseDelete)
      moderatorRemoveStatusValidation(responseDelete.status)
      const response = await adminsService.refreshAdmins();
      setData(response.data);
    });
  };

  const handleCancel = async () => {
    setIsVisible(false);
    form.setFieldsValue({ login: "", password: "" });
  };

  const columns = [
    {
      title: "Id",
      dataIndex: "id",
      rowKey: "id",
      key: "id",
    },
    {
      title: "Login",
      dataIndex: "login",
      rowKey: "login",
      key: "login",
    },
    {
      title: "Role",
      dataIndex: "role",
      rowKey: "role",
      key: "role",
    },
    {
      title: "Операции",
      dataIndex: "operation",
      width: "100px",
      render: (_: any, record: IAdmin) => {
        return  <TableActionWrapper>
          <Typography.Link
              onClick={() => edit(record)}
          >
            <Tooltip title="Изменить модератора" color="#196354">
              <EditFilled />
            </Tooltip>
          </Typography.Link>
          <Typography.Link
              onClick={() => deleteRecord(record)}
          >
            <Tooltip title="Удалить ы" color="#196354">
              <DeleteFilled />
            </Tooltip>
          </Typography.Link>
        </TableActionWrapper>
      },
    },
  ];

  return (
    <>
      <Button style={{ marginBottom: 20 }} onClick={add}>
        Добавить модератора
      </Button>
      <Table
        bordered
        dataSource={data}
        rowKey={(record) => record.id}
        columns={columns}
      />
      <OperationModal
        form={form}
        onFinishAdd={onFinishAdd}
        handleCancel={handleCancel}
        isVisible={isVisible}
        editId={editId}
        onFinishEdit={onFinishEdit}
      />
    </>
  );
};

export default AdminTable;
