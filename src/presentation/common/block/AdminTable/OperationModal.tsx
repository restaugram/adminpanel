import React from "react";
import {Button, Form, Input, Modal} from "antd";
import styled from "styled-components";

const ButtonWrapper = styled.div`
  justify-content: space-around;
  display: inline-flex;
  width: 100%;
`


interface IAdminModal {
  isVisible: boolean;
  handleCancel: () => {};
  onFinishAdd: any;
  form: any;
  editId: string;
  onFinishEdit: any;
}

const OperationModal = ({
  isVisible,
  handleCancel,
  onFinishAdd,
  form,
  editId,
  onFinishEdit,
}: IAdminModal) => {
  return (
    <Modal
      visible={isVisible}
      onCancel={handleCancel}
      footer={<></>}
      title={editId !== null ? "Moderator edit" : "Moderator add"}
    >
      <Form
        form={form}
        onFinish={editId !== null ? onFinishEdit : onFinishAdd}
      >
            <Form.Item
                name="login"
                label={"Login"}
                rules={[
                  {
                    required: true,
                    type: "string",
                    min: 3,
                    max: 200,
                    message: "Please enter the login for moderator",
                  },
                ]}
            >
              <Input placeholder="Login" />
            </Form.Item>
            <Form.Item
                name="password"
                label={"Password"}

                rules={[
                  {
                    required: true,
                    min: 6,
                    max: 16,
                    message:
                        "Please enter a password",
                  },
                ]}
            >
                <Input.Password placeholder={"Password"} />
            </Form.Item>
          <ButtonWrapper>
            <Button onClick={handleCancel} className="ps-btn">
              Cancel
            </Button>
            <Button htmlType={"submit"} className="ps-btn">
              {editId !== null ? "Edit" : "Save"}
            </Button>
          </ButtonWrapper>
      </Form>
    </Modal>
  );
};

export default OperationModal;
