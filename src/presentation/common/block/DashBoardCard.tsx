import {Card, Typography} from "antd";
import styled from "styled-components";

const ValueWrapper = styled.p`
  font-weight: bold;
  font-size: 32pt;
`

const CardWrapper = styled(Card)`
  border-radius: 10px;
  width: 100%;
  max-height: 135px;
  border: 2px solid #DFE0EB;
  text-align: center;
  padding: 0 20px;
`

interface IDashBoardCard{
    title: string,
    value: any
}

const DashBoardCard = ({title, value} : IDashBoardCard) => {
    return(
        <CardWrapper>
            <Typography.Title type={"secondary"} level={5}>{title}</Typography.Title>
            <ValueWrapper>{value}</ValueWrapper>
        </CardWrapper>
    )
}

export default DashBoardCard