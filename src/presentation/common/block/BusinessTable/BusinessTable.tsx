import React, { useState } from "react";
import {
    Table,
    Typography,
    Tooltip,
} from "antd";
import { DeleteFilled } from "@ant-design/icons";
import styled from "styled-components";

import {IBusiness} from "domain/interfaces/IBusiness";
import {businessService} from "data/API";
import {businessRemoveStatusValidation} from "helper/responseStatus";

const TableActionWrapper = styled.div`
  display: inline-flex;
  justify-content: space-around;
  font-size: 20px;
  width: 100%;
`;

interface IBusinessTable {
    business: Array<IBusiness>;
}

const BusinessTable = ({ business }: IBusinessTable) => {
    const [data, setData] = useState(business);

    const deleteRecord = async (record: Partial<IBusiness>) => {
        await businessService.deleteBusinessApi(record.id).then(async (responseDelete) => {
            console.log(responseDelete)
            businessRemoveStatusValidation(responseDelete.status)
            const response = await businessService.refreshBusiness();
            setData(response.data);
        });
    };

    const columns = [
        {
            title: "Id",
            dataIndex: "id",
            rowKey: "id",
            key: "id",
        },
        {
            title: "Business Name",
            dataIndex: "businessName",
            rowKey: "businessName",
            key: "businessName",
        },
        {
            title: "Business Email",
            dataIndex: "businessEmail",
            rowKey: "businessEmail",
            key: "businessEmail",
        },
        {
            title: "Description",
            dataIndex: "description",
            rowKey: "description",
            key: "description",
        },
        {
            title: "Registration Date",
            dataIndex: "registrationDate",
            rowKey: "registrationDate",
            key: "registrationDate",
        },
        {
            title: "Category Id",
            dataIndex: "categoryId",
            rowKey: "categoryId",
            key: "categoryId",
        },
        {
            title: "Операции",
            dataIndex: "operation",
            width: "100px",
            render: (_: any, record: IBusiness) => {
                return  <TableActionWrapper>
                    <Typography.Link
                        onClick={() => deleteRecord(record)}
                    >
                        <Tooltip title="Удалить бизнес" color="#196354">
                            <DeleteFilled />
                        </Tooltip>
                    </Typography.Link>
                </TableActionWrapper>
            },
        },
    ];

    return (
        <>
            <Table
                bordered
                dataSource={data}
                rowKey={(record) => record.id}
                columns={columns}
            />
        </>
    );
};

export default BusinessTable;
