import Image from "next/image"
import styled from "styled-components";
import { Typography } from 'antd';

const LogoWrapper = styled.div`
  display: flex;
  justify-content: center;
  flex-direction:  ${props => props.isHorizontal ? "row" : "column"};
  align-items: center;
  margin-top: 40px;
  margin-bottom: 30px;
`

interface ILogo{
    isHorizontal: boolean,
    isCollapsed?: boolean
    src: string,
    text: string,
}

const Logo = ({isHorizontal, isCollapsed = false, src, text} : ILogo) => {
    return(
        <LogoWrapper isHorizontal={isHorizontal}>
            <Image quality={100} width={isHorizontal ? 32 : 48} height={isHorizontal ? 32 : 48} src={src} alt={"logo"}/>
            {isCollapsed ? null : <Typography.Title style={isHorizontal ? {marginBottom: 0, marginLeft: 10} : {marginBottom: 0}} level={isHorizontal ? 4 : 3} type={"secondary"}>{text}</Typography.Title>}
        </LogoWrapper>
    )
}

export default Logo