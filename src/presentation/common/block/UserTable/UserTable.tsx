import React, { useState } from "react";
import {
    Table,
    Typography,
    Tooltip,
} from "antd";
import { DeleteFilled } from "@ant-design/icons";
import styled from "styled-components";

import { userService } from "data/API";
import { userRemoveStatusValidation} from "helper/responseStatus";
import {IUser} from "domain/interfaces/IUser";

const TableActionWrapper = styled.div`
  display: inline-flex;
  justify-content: space-around;
  font-size: 20px;
  width: 100%;
`;

interface IUserTable {
    users: Array<IUser>;
}

const UserTable = ({ users }: IUserTable) => {
    const [data, setData] = useState(users);
    console.log(users)

    const deleteRecord = async (record: Partial<IUser>) => {
        await userService.deleteUserApi(record.id).then(async (responseDelete) => {
            console.log(responseDelete)
            userRemoveStatusValidation(responseDelete.status)
            const response = await userService.refreshUsers();
            setData(response.data);
        });
    };

    const columns = [
        {
            title: "Id",
            dataIndex: "id",
            rowKey: "id",
            key: "id",
        },
        {
            title: "Full Name",
            dataIndex: "fullName",
            rowKey: "fullName",
            key: "fullName",
        },
        {
            title: "Email",
            dataIndex: "email",
            rowKey: "email",
            key: "email",
        },
        {
            title: "Phone",
            dataIndex: "phone",
            rowKey: "phone",
            key: "phone",
        },
        {
            title: "Rating",
            dataIndex: "rating",
            rowKey: "rating",
            key: "rating",
        },
        {
            title: "Birth Date",
            dataIndex: "birthDate",
            rowKey: "birthDate",
            key: "birthDate",
        },
        {
            title: "Операции",
            dataIndex: "operation",
            width: "100px",
            render: (_: any, record: IUser) => {
                return  <TableActionWrapper>
                    <Typography.Link
                        onClick={() => deleteRecord(record)}
                    >
                        <Tooltip title="Удалить пользователя" color="#196354">
                            <DeleteFilled />
                        </Tooltip>
                    </Typography.Link>
                </TableActionWrapper>
            },
        },
    ];

    return (
        <>
            <Table
                bordered
                dataSource={data}
                rowKey={(record) => record.id}
                columns={columns}
            />
        </>
    );
};

export default UserTable;
