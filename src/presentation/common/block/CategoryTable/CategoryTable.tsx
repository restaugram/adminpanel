import React, { useState } from "react";
import {
  Table,
  Form,
  Typography,
  Tooltip,
  Image,
  Button,
} from "antd";
import { EditFilled, DeleteFilled } from "@ant-design/icons";
import styled from "styled-components";

import { ICategory } from "domain/interfaces/ICategory";
import { categoriesService } from "data/API";
import OperationModal from "./OperationModal";
import {
  categoryAddStatusValidation,
  categoryEditStatusValidation,
  categoryRemoveStatusValidation
} from "helper/responseStatus";

const TableActionWrapper = styled.div`
  display: inline-flex;
  justify-content: space-around;
  font-size: 20px;
  width: 100%;
`;

interface ICategoryTable {
  categories: Array<ICategory>;
}

const CategoryTable = ({ categories }: ICategoryTable) => {
  const [form] = Form.useForm();
  const [isVisible, setIsVisible] = useState(false);
  const [editId, setEditId] = useState<any>(null);

  const [data, setData] = useState(categories);

  const onFinishAdd = async (values: any) => {
    await categoriesService
      .categoryCreateApi(
        values.imageUrl,
        values.name,
        values.parentId,
        values.slug
      )
      .then(async (responseAdd) => {
        categoryAddStatusValidation(responseAdd.status)
        const response = await categoriesService.refreshCategories();
        setData(response.data);
        setIsVisible(false);
        form.setFieldsValue({ name: "", slug: "", imageUrl: "", parentId: "" });
      });
  };

  const onFinishEdit = async (values: any) => {
    console.log(values)
    await categoriesService
      .editCategoryApi(
        editId,
        values.imageUrl,
        values.name,
        values.parentId,
        values.slug
      )
      .then(async (responseEdit) => {
        categoryEditStatusValidation(responseEdit)
        const response = await categoriesService.refreshCategories();
        setData(response.data);
        setIsVisible(false);
        form.setFieldsValue({ name: "", slug: "", imageUrl: "", parentId: "" });
      });
  };


  const add = () => {
    setEditId(null);
    form.setFieldsValue({parentId: "00000000-0000-0000-0000-000000000000"})
    setIsVisible(true);
  };

  const edit = (record: Partial<ICategory>) => {
    form.setFieldsValue({
      name: record.name,
      slug: record.slug,
      imageUrl: record.imageUrl,
      parentId: record.parentId,
    });
    setEditId(record.id);
    setIsVisible(true);
  };

  const deleteRecord = async (record: Partial<ICategory>) => {
    await categoriesService.deleteCategoryApi(record.id).then(async (responseDelete) => {
      console.log(responseDelete)
      categoryRemoveStatusValidation(responseDelete.status)
      const response = await categoriesService.refreshCategories();
      setData(response.data);
    });
  };

  const handleCancel = async () => {
    setIsVisible(false);
    form.setFieldsValue({ name: "", slug: "", imageUrl: "", parentId: "" });
  };

  const columns = [
    {
      title: "Id",
      dataIndex: "id",
      rowKey: "id",
      key: "id",
    },
    {
      title: "Image",
      dataIndex: "imageUrl",
      rowKey: "imageUrl",
      key: "imageUrl",
      render: (image) => (
        <Image
          src={image !== null ? image : "/static/images/not-found.jpg"}
          alt={image}
          fallback="/static/images/not-found.jpg"
        />
      ),
    },
    {
      title: "Name",
      dataIndex: "name",
      rowKey: "name",
      key: "name",
    },
    {
      title: "Slug",
      dataIndex: "slug",
      rowKey: "slug",
      key: "slug",
    },
    {
      title: "Операции",
      dataIndex: "operation",
      width: "100px",
      render: (_: any, record: ICategory) => {
        return  <TableActionWrapper>
          <Typography.Link
              onClick={() => edit(record)}
          >
            <Tooltip title="Изменить адрес" color="#196354">
              <EditFilled />
            </Tooltip>
          </Typography.Link>
          <Typography.Link
              onClick={() => deleteRecord(record)}
          >
            <Tooltip title="Удалить адрес" color="#196354">
              <DeleteFilled />
            </Tooltip>
          </Typography.Link>
        </TableActionWrapper>
      },
    },
  ];

  return (
    <>
      <Button style={{ marginBottom: 20 }} onClick={add}>
        Добавить категорию
      </Button>
      <Table
        bordered
        dataSource={data}
        rowKey={(record) => record.id}
        columns={columns}
      />
      <OperationModal
        form={form}
        onFinishAdd={onFinishAdd}
        handleCancel={handleCancel}
        isVisible={isVisible}
        editId={editId}
        onFinishEdit={onFinishEdit}
      />
    </>
  );
};

export default CategoryTable;
