import React from "react";
import {Button, Form, Input, Modal} from "antd";
import styled from "styled-components";

const ButtonWrapper = styled.div`
  justify-content: space-around;
  display: inline-flex;
  width: 100%;
`


interface ICategoryModal {
  isVisible: boolean;
  handleCancel: () => {};
  onFinishAdd: any;
  form: any;
  editId: string;
  onFinishEdit: any;
}

const OperationModal = ({
  isVisible,
  handleCancel,
  onFinishAdd,
  form,
  editId,
  onFinishEdit,
}: ICategoryModal) => {
  return (
    <Modal
      visible={isVisible}
      onCancel={handleCancel}
      footer={<></>}
      title={editId !== null ? "Category edit" : "Category add"}
    >
      <Form
        form={form}
        onFinish={editId !== null ? onFinishEdit : onFinishAdd}
      >
            <Form.Item
                name="name"
                label={"Name"}
                rules={[
                  {
                    required: true,
                    type: "string",
                    min: 3,
                    max: 200,
                    message: "Please enter the name of the categories",
                  },
                ]}
            >
              <Input placeholder="Name" />
            </Form.Item>
            <Form.Item
                name="slug"
                label={"Slug"}
                rules={[
                  {
                    required: true,
                    type: "string",
                    pattern: /([a-z])/,
                    min: 3,
                    max: 200,
                    whitespace: false,
                    message: "Please enter slug categories",
                  },
                ]}
            >
              <Input placeholder="Slug" />
            </Form.Item>
            <Form.Item
                name="parentId"
                label={"Parent Id"}
                initialValue="00000000-0000-0000-0000-000000000000"
                rules={[
                  {
                    required: true,
                    type: "string",
                    min: 36,
                    max: 36,
                    message: "Please enter the guid parentId of the categories",
                  },
                ]}
            >
              <Input placeholder="Guid of parrent category" />
            </Form.Item>
            <Form.Item
                name="imageUrl"
                label={"Image Url"}
                rules={[
                  {
                    required: true,
                    type: "url",
                    min: 10,
                    max: 500,
                    message:
                        "Please enter a link to the image of the categories",
                  },
                ]}
            >
              <Input placeholder="Url of category image" />
            </Form.Item>
          <ButtonWrapper>
            <Button onClick={handleCancel} className="ps-btn">
              Cancel
            </Button>
            <Button htmlType={"submit"} className="ps-btn">
              {editId !== null ? "Edit" : "Save"}
            </Button>
          </ButtonWrapper>
      </Form>
    </Modal>
  );
};

export default OperationModal;
