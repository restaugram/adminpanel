import {useEffect, useState} from "react";
import {useRouter} from "next/router";
import {Menu} from "antd"
import styled from "styled-components";

import {IMenuItem} from "domain/interfaces/system/IMenuItem";
const { SubMenu } = Menu;

interface IMenu{
    source: Array<IMenuItem>,
    mode: "horizontal" | "vertical" | "inline",
    theme: "light" | "dark"
}

const SiderMenu = styled(Menu)`
  font-size: 12pt;
  background-color: #363740;
`

const LayoutMenu = ({ source, mode, theme } : IMenu) => {
    const [current, setCurrent] = useState(source[0].id);
    const Router = useRouter()

    useEffect(() => {
        const currentIndex = source.findIndex(item => item.slug === Router.pathname)
        setCurrent(source[currentIndex].id)
    }, [Router.pathname])

    const handleClick = async (e, slug) => {
        setCurrent(e.key)
        await Router.push({
                pathname: slug,
            }, undefined,
            { shallow: false, scroll: false })
    };

    const renderMultiple = (item : IMenuItem) => {
        if(item.children.length != 0)
            return(
                <SubMenu icon={item.icon} onTitleClick={e => handleClick(e, item.slug)} key={item.id} title={item.name}>
                    {item.children?.map(itemDropDown => {
                        if(itemDropDown.children.length != 0)
                            return renderMultiple(itemDropDown)
                        else
                            return(<Menu.Item icon={itemDropDown.icon} key={itemDropDown.id}>
                                {itemDropDown.name}
                            </Menu.Item>)
                    })}
                </SubMenu>
            )
        else
            return(
                <Menu.Item onClick={e => handleClick(e, item.slug)} icon={item.icon} key={item.id}>
                    {item.name}
                </Menu.Item>
            )
    }

    // Views
    let menuView;

    if (source) {
        menuView = source.map((item) => {
            return renderMultiple(item)
        });
    } else {
        menuView = (
            <Menu.Item>Здесь нет меню</Menu.Item>
        );
    }
    return <SiderMenu theme={theme} selectedKeys={[current]} mode={mode}>{menuView}</SiderMenu>;
};

export default LayoutMenu;