import {Row, Col, Typography, Form, Button} from 'antd';
import {useDispatch} from "react-redux";
import styled from "styled-components";

import {login} from "data/stores/actions/systemAccountActions";
import Logo from "../common/block/Logo";
import {
    CustomInput,
    CustomInputPassword,
    InputComponentWrapper,
    InputLabel
} from "../common/control/Input";
import {useRouter} from "next/router";

const LoginPageWrapper = styled(Row)`
  display: flex;
  justify-content: center;
`

const LoginPageFormWrapper = styled(Col)`
  margin-top: 15vh;
  background-color: #ffffff;
  width: 100%;
  display: block;
  text-align: center;
  border-radius: 20px;
`

const LoginForm = styled(Form)`
  padding: 0 30px;
  text-align: initial;
`

const LoginButton = styled(Button)`
  border-radius: 5px;
  font-size: 12pt;
  width: 50%;
`

const LoginPage = () => {
    const dispatch = useDispatch();
    const Router = useRouter()

    const onFinish = async (values: any) => {
       console.log(values)
       dispatch(login(values.login, values.password, Router))
    };


    return (
        <LoginPageWrapper>
            <LoginPageFormWrapper xs={22} sm={18} md={16} lg={10} xl={10}>
                <Logo isHorizontal={false} src={"/static/images/logo.png"} text={"Dashboard Kit"}/>
                <Typography.Title style={{marginBottom: 5}} level={2}>Log In to Dashboard Kit</Typography.Title>
                <Typography.Title style={{marginTop: 5}} level={4} type={"secondary"}>Enter your username and password below</Typography.Title>
                <LoginForm onFinish={onFinish}>
                    <Form.Item
                        style={{marginBottom: 20}}
                        name="login"
                        rules={[
                            {
                                required: true,
                                message:
                                    'Пожалуйста, введите ваш логин'
                            },
                        ]}
                    >
                        <InputComponentWrapper>
                            <InputLabel>Login</InputLabel>
                            <CustomInput placeholder={"Login"} />
                        </InputComponentWrapper>
                    </Form.Item>
                    <Form.Item
                        name="password"
                        rules={[
                            {
                                required: true,
                                message:
                                    'Пожалуйста, введите пароль'
                            },
                            {
                                // pattern: /^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[!@#$%^&*])(?=.{8,})/i,
                                // message: 'Пароль должен содержать минимум 8 символов и содержать минимум 1 заглавный символ и не 1 знак',
                                min: 6,
                                max: 60,
                                message:
                                    'Пароль должен содержать минимум 6 символов'
                            }
                        ]}
                    >
                        <InputComponentWrapper>
                            <InputLabel>Password</InputLabel>
                            <CustomInputPassword placeholder={"Password"} />
                        </InputComponentWrapper>
                    </Form.Item>
                    <Form.Item style={{textAlign: "center"}}>
                        <LoginButton size="large" type="primary" htmlType="submit">Login</LoginButton>
                    </Form.Item>
                </LoginForm>
            </LoginPageFormWrapper>
        </LoginPageWrapper>
    )
}

export default LoginPage