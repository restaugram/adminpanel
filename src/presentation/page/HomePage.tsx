import styled from "styled-components";
import {Card, Row, Col} from "antd";
import DashBoardCard from "../common/block/DashBoardCard";
import UserTable from "../common/block/UserTable/UserTable";
import {IUser} from "domain/interfaces/IUser";



const TableCard = styled(Card)`
  border-radius: 10px;
  width: 100%;
  min-height: 400px;
  margin-top: 20px;
`

interface IHomePage{
    users: Array<IUser>
}

const HomePage = ({users} : IHomePage) => {
    return(<>
        <Row gutter={{ xs: 8, sm: 16, md: 24, lg: 32 }}>
            <Col span={6}>
                <DashBoardCard title="Unresolved" value={60}/>
            </Col>
            <Col span={6}>
                <DashBoardCard title="Overdue" value={16}/>
            </Col>
            <Col span={6}>
                <DashBoardCard title="Open" value={43}/>
            </Col>
            <Col span={6}>
                <DashBoardCard title="On hold" value={64}/>
            </Col>
        </Row>
        <TableCard>
            <UserTable users={users}/>
        </TableCard>
    </>)
}


export default HomePage