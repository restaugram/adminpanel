import styled from "styled-components";
import {Card} from "antd";
import {IAdmin} from "domain/interfaces/IAdmin";
import AdminTable from "../common/block/AdminTable/AdminTable";

const TableCard = styled(Card)`
  border-radius: 10px;
  width: 100%;
  min-height: 400px;
  margin-top: 20px;
`

interface IAdminsPage{
    admins: Array<IAdmin>
}

const AdminsPage = ({ admins } : IAdminsPage) => {
    return(<>
        <TableCard>
            <AdminTable admins={admins}/>
        </TableCard>
    </>)
}


export default AdminsPage