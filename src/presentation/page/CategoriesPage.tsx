import styled from "styled-components";
import {Card} from "antd";
import {ICategory} from "domain/interfaces/ICategory";
import CategoryTable from "../common/block/CategoryTable/CategoryTable";

const TableCard = styled(Card)`
  border-radius: 10px;
  width: 100%;
  min-height: 400px;
  margin-top: 20px;
`

interface ICategoriesPage{
    categories: Array<ICategory>
}

const CategoriesPage = ({ categories } : ICategoriesPage) => {
    return(<>
        <TableCard>
            <CategoryTable categories={categories}/>
        </TableCard>
    </>)
}


export default CategoriesPage