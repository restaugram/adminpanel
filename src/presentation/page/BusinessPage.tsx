import styled from "styled-components";
import {Card} from "antd";
import BusinessTable from "../common/block/BusinessTable/BusinessTable";
import {IBusiness} from "domain/interfaces/IBusiness";

const TableCard = styled(Card)`
  border-radius: 10px;
  width: 100%;
  min-height: 400px;
  margin-top: 20px;
`

interface IBusinessPage{
    business: Array<IBusiness>
}

const BusinessPage = ({ business } : IBusinessPage) => {
    return(<>
        <TableCard>
            <BusinessTable business={business}/>
        </TableCard>
    </>)
}


export default BusinessPage
