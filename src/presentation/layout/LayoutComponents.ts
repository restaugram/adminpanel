import {Avatar, Badge, Divider, Layout} from "antd";
import styled from "styled-components";

const { Sider, Header, Content } = Layout;


export const SideBar = styled(Sider)`
  background-color: #363740;
`

export const BadgeCustom = styled(Badge)`
  cursor: pointer;
  color: #C5C7CD;
  margin-right: 10px;
`

export const UserAvatar = styled(Avatar)`
  margin-right: 10px;
  margin-left: 10px;
`

export const Divide = styled(Divider)`
  height: 25px;
  border-left: 2px solid rgba(0, 0, 0, 0.06);
  margin: 0 10px;
`


export const HeaderComponent = styled(Header)`
  background-color: inherit;
  padding: 20px 30px;
  display: inline-flex;
  justify-content: space-between;
`

export const HeaderRightWrapper = styled.div`
  display: inline-flex;
`
export const ContentWrapper = styled(Content)`
  padding: 30px 40px;
`