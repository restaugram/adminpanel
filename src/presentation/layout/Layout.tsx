import {ReactElement, useEffect, useState} from "react";
import { Layout, Breadcrumb, Typography } from 'antd';
import { BellFilled, LogoutOutlined } from '@ant-design/icons';
import {useDispatch, useSelector} from "react-redux";

import Logo from "presentation/common/block/Logo";
import LayoutMenu from "presentation/common/layout/LayoutMenu";
import {adminLayoutMenu, layoutMenu} from "data/static/layoutMenu";
import {RootState} from "data/stores/rootReducer";
import {LOG_OUT} from "data/stores/actions";

import { SideBar, HeaderComponent, HeaderRightWrapper,Divide, BadgeCustom, ContentWrapper } from "./LayoutComponents";
import {getUserData} from "data/stores/actions/systemAccountActions";

const { Footer } = Layout;


interface ILayoutComponent{
    breadCrumbs: Array<string>,
    children: ReactElement
}

const LayoutComponent = ({ breadCrumbs, children } : ILayoutComponent) => {

    const [isCollapsed, setIsCollapsed] = useState(true)
    const dispatch = useDispatch()

    useEffect(() => {
        dispatch(getUserData())
    }, [])

    const { login, role } = useSelector((state : RootState) => state.account)

    const onCollapse = () => {
        setIsCollapsed(!isCollapsed)
    }

    return(
        <Layout style={{ minHeight: '100vh' }}>
            <SideBar width={255} collapsible collapsed={isCollapsed} onCollapse={onCollapse}>
                <Logo isCollapsed={isCollapsed} isHorizontal={true} src={"/static/images/logo.png"} text={"Dashboard Kit"}/>
                {role !== "" ? (<LayoutMenu theme={"dark"} source={role === 'SuperAdmin' ? [...layoutMenu, ...adminLayoutMenu] : layoutMenu} mode={"vertical"}/>) : null}
            </SideBar>
            <Layout>
                <HeaderComponent>
                    <Breadcrumb>
                        {breadCrumbs.map((item, key) => (
                            <Breadcrumb.Item key={key}>
                                <Typography.Title level={3}>{item}</Typography.Title>
                            </Breadcrumb.Item>
                        ))}
                    </Breadcrumb>
                    <HeaderRightWrapper>

                        <LogoutOutlined onClick={() => dispatch({type: LOG_OUT})}
                                        style={{fontSize: 20, color: "#C5C7CD", marginRight: 10}} />
                        <BadgeCustom style={{backgroundColor: "#3751FF"}} dot={true}>
                            <BellFilled style={{fontSize: 20}} />
                        </BadgeCustom>
                        <Divide type={"vertical"}/>
                        <Typography.Title level={5}>{login} | {role}</Typography.Title>
{/*
                        <UserAvatar size="large" icon={<UserOutlined />} />
*/}
                    </HeaderRightWrapper>
                </HeaderComponent>
                <ContentWrapper>
                    {children}
                </ContentWrapper>
                <Footer style={{ textAlign: 'center' }}>Bookingo 2022@ Created by Bookingo</Footer>
            </Layout>
        </Layout>
    )
}

export default LayoutComponent
