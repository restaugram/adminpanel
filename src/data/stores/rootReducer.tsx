import {createStore, applyMiddleware, combineReducers} from 'redux'
import thunk from 'redux-thunk'
import {systemAccountReducer} from "./reducers/systemAccountReducer";
import {categoryReducer} from "./reducers/categoryReducer";


const rootReducer = combineReducers({
    account: systemAccountReducer,
    category: categoryReducer
})

const store = createStore(rootReducer, applyMiddleware(thunk))


export type RootState = ReturnType<typeof rootReducer>
export default store;
