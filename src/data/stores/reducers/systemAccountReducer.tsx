import {AnyAction} from "redux";

import {logOut} from "helper/userHelper";
import {LOG_IN, LOG_OUT} from "../actions";

export interface systemAccountReducer{
    isAuth: boolean,
    login: string,
    role: string | null
}

const initialState : systemAccountReducer = {
    isAuth: false,
    login: "",
    role: ""
}

export function systemAccountReducer(state = initialState, action: AnyAction) {
    switch (action.type) {
        case LOG_IN:
            console.log(action)
            return {...state, isAuth: action.payload.isAuth, login: action.payload.login, role: action.payload.role}
        case LOG_OUT:
            logOut()
            return {...state, initialState}
        default:
            return state;
    }
}

