import {AnyAction} from "redux";

import {ADD_CATEGORY, DELETE_CATEGORY, EDIT_CATEGORY, GET_CATEGORIES} from "../actions";
import {ICategory} from "domain/interfaces/ICategory";

export interface ICategoryReducer{
    categories: Array<ICategory> | []
}

const initialState : ICategoryReducer = {
    categories: [],
}

export function categoryReducer(state = initialState, action: AnyAction) {
    switch (action.type) {
        case GET_CATEGORIES:
            return {...state, categories: action.payload.categories}
        case ADD_CATEGORY:
            return {...state, categories: [...state.categories, action.payload.category]}
        case EDIT_CATEGORY:
            return {...state, categories: action.payload.categories}
        case DELETE_CATEGORY:
            return {...state, categories: action.payload.categories}
        default:
            return state;
    }
}

