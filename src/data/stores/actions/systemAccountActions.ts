import Cookies from "js-cookie";
import {NextRouter} from "next/router";

import {USER_FIRST_PART, USER_ID, USER_ROLE, USER_SECOND_PART, USER_THIRD_PART} from "data/static/storageNames";
import {authorizationStatusValidation} from "helper/responseStatus";
import {authenticationService} from "data/API";
import {HOME} from "data/static/routes";
import {LOG_IN} from "../actions";


export function login(login: string, password: string, Router: NextRouter) {
    return async dispatch => {
        const response = await authenticationService.adminSignInApi(login, password)
        const token = response.data.token;
        authorizationStatusValidation(response.status)
        if (response.status === 200) {
            //JWT token split and save in cookies
            const split = token.split('.');
            Cookies.set(USER_FIRST_PART, split[0], {expires: 7});
            Cookies.set(USER_SECOND_PART, split[1], {expires: 7});
            Cookies.set(USER_THIRD_PART, split[2], {expires: 7});
            //request to get data of authenticated user
            const adminResponse = await authenticationService.getAdminApi();
            //Save role of user and id in cookies
            Cookies.set(USER_ROLE, adminResponse.data.role)
            Cookies.set(USER_ID, adminResponse.data.id)
            //dispatch set of authenticated user data in our redux store
            dispatch({ type: LOG_IN, payload: {isAuth: true, login: adminResponse.data.login, role: adminResponse.data.role}})
            //redirect to Home
            await Router.push(HOME)
        }
    }
}

export function getUserData(){
    return async dispatch => {
        const adminResponse = await authenticationService.getAdminApi();
        //Save role of user and id in cookies
        Cookies.set(USER_ROLE, adminResponse.data.role)
        Cookies.set(USER_ID, adminResponse.data.id)
        dispatch({ type: LOG_IN, payload: {isAuth: true, login: adminResponse.data.login, role: adminResponse.data.role}})
    }
}
