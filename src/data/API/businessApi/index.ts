import axios from 'axios';
import { BackendUrl } from '../settings';
import {getUser} from "helper/userHelper";

axios.defaults.withCredentials = true;


const getBusinessApi = async (jwtToken : string) => {
	return await axios.get(`${BackendUrl}/business`,
		{headers: { Authorization: `Bearer ${jwtToken}` }}).then(response => {
		return response
	}).catch(error => {
		return error.response
	})
}

const refreshBusiness = async () => {
	return await axios
		.get(`${BackendUrl}/business`, {
			headers: { Authorization: `Bearer ${getUser()}` }
		})
		.then((response) => {
			return response;
		})
		.catch((error) => {
			return error.response;
		});
};


const getBusinessByIdApi = async (id: string) => {
	return await axios.get(`${BackendUrl}/business/${id}`,
		{headers: { Authorization: `Bearer ${getUser()}` }}).then(response => {
		return response
	}).catch(error => {
		return error.response
	})
}

const deleteBusinessApi = async (id: string | undefined) => {
	return await axios.delete(`${BackendUrl}/business/${id}`,
		{headers: { Authorization: `Bearer ${getUser()}` }}).then(response => {
		return response
	}).catch(error => {
		return error.response
	})
}

export const businessService = {
	getBusinessApi,
	refreshBusiness,
	getBusinessByIdApi,
	deleteBusinessApi
};
