import axios from 'axios';
import { BackendUrl } from '../settings';
import {getUser} from "helper/userHelper";

axios.defaults.withCredentials = true;

const adminSignInApi = async (login: string, password: string) => {
	console.log(password)
	return await axios
		.post(`${BackendUrl}/identity`, {
			login: login,
			password: password
		})
		.then((response) => {
			console.log(response)
			return response;
		})
		.catch((error) => {
			return error.response;
		});
};

const getAdminApi = async () => {
	return await axios.get(`${BackendUrl}/identity`,
		{headers: { Authorization: `Bearer ${getUser()}` }}).then(response => {
		return response
	}).catch(error => {
		return error.response
	})
}

export const authenticationService = {
	adminSignInApi,
	getAdminApi
};
