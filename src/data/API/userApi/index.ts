import axios from 'axios';
import { BackendUrl } from '../settings';
import {getUser} from "helper/userHelper";

axios.defaults.withCredentials = true;


const getUsersApi = async (jwtToken : string) => {
	return await axios.get(`${BackendUrl}/users`,
		{headers: { Authorization: `Bearer ${jwtToken}` }}).then(response => {
		return response
	}).catch(error => {
		return error.response
	})
}

const refreshUsers = async () => {
	return await axios
		.get(`${BackendUrl}/users`, {
			headers: { Authorization: `Bearer ${getUser()}` }
		})
		.then((response) => {
			return response;
		})
		.catch((error) => {
			return error.response;
		});
};


const getUserByIdApi = async (id: string) => {
	return await axios.get(`${BackendUrl}/users/${id}`,
		{headers: { Authorization: `Bearer ${getUser()}` }}).then(response => {
		return response
	}).catch(error => {
		return error.response
	})
}

const deleteUserApi = async (id: string | undefined) => {
	return await axios.delete(`${BackendUrl}/users/${id}`,
		{headers: { Authorization: `Bearer ${getUser()}` }}).then(response => {
		return response
	}).catch(error => {
		return error.response
	})
}

export const userService = {
	getUsersApi,
	refreshUsers,
	getUserByIdApi,
	deleteUserApi
};
