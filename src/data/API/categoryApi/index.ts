import axios from 'axios';
import { BackendUrl } from '../settings';
import {getUser} from "helper/userHelper";

axios.defaults.withCredentials = true;


const getCategoriesApi = async (jwtToken : string) => {
	return await axios.get(`${BackendUrl}/categories`,
		{headers: { Authorization: `Bearer ${jwtToken}` }}).then(response => {
		return response
	}).catch(error => {
		return error.response
	})
}

const refreshCategories = async () => {
	return await axios
		.get(`${BackendUrl}/categories`, {
			headers: { Authorization: `Bearer ${getUser()}` }
		})
		.then((response) => {
			return response;
		})
		.catch((error) => {
			return error.response;
		});
};


const categoryCreateApi = async (imageUrl: string, name: string, parentId: string, slug: string) => {
	return await axios
		.post(`${BackendUrl}/categories`, {
			imageUrl: imageUrl,
			name: name,
			parentId: parentId,
			slug: slug
		}, {headers: { Authorization: `Bearer ${getUser()}` }})
		.then((response) => {
			return response;
		})
		.catch((error) => {
			return error.response;
		});
};


const getCategoryByIdApi = async (id: string) => {
	return await axios.get(`${BackendUrl}/categories/${id}`,
		{headers: { Authorization: `Bearer ${getUser()}` }}).then(response => {
		return response
	}).catch(error => {
		return error.response
	})
}

const editCategoryApi = async (id: string, imageUrl: string, name: string, parentId: string, slug: string) => {
	console.log(id)
	console.log(name)
	return await axios
		.put(`${BackendUrl}/categories/${id}`, {
			imageUrl: imageUrl,
			name: name,
			parentId: parentId,
			slug: slug
		}, {headers: { Authorization: `Bearer ${getUser()}` }})
		.then((response) => {
			console.log(response)
			return response;
		})
		.catch((error) => {
			return error.response;
		});
}

const deleteCategoryApi = async (id: string | undefined) => {
	return await axios.delete(`${BackendUrl}/categories/${id}`,
		{headers: { Authorization: `Bearer ${getUser()}` }}).then(response => {
		return response
	}).catch(error => {
		return error.response
	})
}

export const categoriesService = {
	getCategoriesApi,
	refreshCategories,
	categoryCreateApi,
	getCategoryByIdApi,
	editCategoryApi,
	deleteCategoryApi
};
