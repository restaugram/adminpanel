import axios from 'axios';
import { BackendUrl } from '../settings';
import {getUser} from "helper/userHelper";

axios.defaults.withCredentials = true;


const getAdminsApi = async (jwtToken : string) => {
	return await axios.get(`${BackendUrl}`,
		{headers: { Authorization: `Bearer ${jwtToken}` }}).then(response => {
		return response
	}).catch(error => {
		return error.response
	})
}

const refreshAdmins = async () => {
	return await axios
		.get(`${BackendUrl}`, {
			headers: { Authorization: `Bearer ${getUser()}` }
		})
		.then((response) => {
			return response;
		})
		.catch((error) => {
			return error.response;
		});
};


const adminAddApi = async (login:string, password:string) => {
	return await axios
		.post(`${BackendUrl}`, {
			login: login,
			password: password
		}, {headers: { Authorization: `Bearer ${getUser()}` }})
		.then((response) => {
			return response;
		})
		.catch((error) => {
			return error.response;
		});
};


const getAdminById = async (id: string) => {
	return await axios.get(`${BackendUrl}/admin/${id}`,
		{headers: { Authorization: `Bearer ${getUser()}` }}).then(response => {
		return response
	}).catch(error => {
		return error.response
	})
}

const editAdminApi = async (id:string, login:string, password:string) => {
	return await axios
		.put(`${BackendUrl}/${id}`, {
			login: login,
			password: password
		}, {headers: { Authorization: `Bearer ${getUser()}` }})
		.then((response) => {
			console.log(response)
			return response;
		})
		.catch((error) => {
			return error.response;
		});
}

const deleteAdminApi = async (id: string | undefined) => {
	return await axios.delete(`${BackendUrl}/${id}`,
		{headers: { Authorization: `Bearer ${getUser()}` }}).then(response => {
		return response
	}).catch(error => {
		return error.response
	})
}

export const adminsService = {
	getAdminsApi,
	refreshAdmins,
	adminAddApi,
	getAdminById,
	editAdminApi,
	deleteAdminApi
};
