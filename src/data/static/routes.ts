//Common --- without authentication
export const HOME = '/';
export const LOGIN = '/login';
export const ADMINS = '/admins';
export const BUSINESS = '/business';
export const CATEGORIES = '/categories';
