import {
    ProfileOutlined,
    IdcardOutlined,
    DashboardOutlined,
    BankOutlined
} from '@ant-design/icons';
import {IMenuItem} from "domain/interfaces/system/IMenuItem";
import {ADMINS, BUSINESS, CATEGORIES, HOME} from "./routes";

export const layoutMenu : Array<IMenuItem> = [
    {
        id: "home",
        icon: (<DashboardOutlined style={{ fontSize: '20px' }}/>),
        children: [],
        slug: HOME,
        name: "Home"
    },
    {
        id: "business",
        icon: (<BankOutlined style={{ fontSize: '20px' }}/>),
        children: [],
        slug: BUSINESS,
        name: "Businesses"
    },
    {
        id: "category",
        icon: (<ProfileOutlined  style={{ fontSize: '20px' }}/>),
        children: [],
        slug: CATEGORIES,
        name: "Categories",
    },
]

export const adminLayoutMenu : Array<IMenuItem> = [
    {
        id: "admins",
        icon: (<IdcardOutlined  style={{ fontSize: '20px' }}/>),
        children: [],
        slug: ADMINS,
        name: "Admins",
    },
]
