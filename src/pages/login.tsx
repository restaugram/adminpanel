import LoginPage from "presentation/page/LoginPage"
import {USER_FIRST_PART, USER_SECOND_PART, USER_THIRD_PART} from "data/static/storageNames";

const Login = () => {

    return(
        <LoginPage/>
    )
}

export async function getServerSideProps({ locale, req }: any) {
    const { cookies } = req;

    if (
        cookies[USER_FIRST_PART] !== undefined &&
        cookies[USER_SECOND_PART] !== undefined &&
        cookies[USER_THIRD_PART] !== undefined
    ) {
        return {
            redirect: {
                destination: '/',
                permanent: false,
            },
        }
    }
    return{
        props: {}
    }

}


export default Login;
