import {USER_FIRST_PART, USER_SECOND_PART, USER_THIRD_PART} from "data/static/storageNames";
import {ICategory} from "domain/interfaces/ICategory";
import {categoriesService} from "data/API";

import CategoriesPage from "presentation/page/CategoriesPage";
import Layout from "presentation/layout/Layout";

const breadCrumbs = [
    "Dashboard"
]

interface ICategories{
    categories: Array<ICategory>
}

const Categories = ({categories} : ICategories) => {
    return (
        <Layout breadCrumbs={breadCrumbs}>
            <CategoriesPage categories={categories} />
        </Layout>
    )
}

export async function getServerSideProps({ locale, req }: any) {
    const { cookies } = req;
    if (
        cookies[USER_FIRST_PART] === undefined &&
        cookies[USER_SECOND_PART] === undefined &&
        cookies[USER_THIRD_PART] === undefined
    ) {
        return {
            redirect: {
                destination: '/login',
                permanent: false,
            },
        }
    }

    const response = await categoriesService.getCategoriesApi(cookies[USER_FIRST_PART] +
        '.' +
        cookies[USER_SECOND_PART] +
        '.' +
        cookies[USER_THIRD_PART]
    );
    return{
        props: {
            categories: response.data !== undefined ? response.data : []
        }
    }

}

export default Categories
