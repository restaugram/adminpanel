import React from "react";
import {Provider} from "react-redux";

import "styles/themes/theme.less"

import mainStore from "data/stores/rootReducer"

function MyApp({Component, pageProps: { session, ...pageProps }}) {

  return (
      <React.StrictMode>
          <Provider store={mainStore}>
              <Component {...pageProps} />
          </Provider>
      </React.StrictMode>
  )
}




export default MyApp
