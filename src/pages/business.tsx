import Layout from "presentation/layout/Layout";
import {USER_FIRST_PART, USER_SECOND_PART, USER_THIRD_PART} from "data/static/storageNames";
import {businessService} from "data/API";

import {IBusiness} from "domain/interfaces/IBusiness";
import BusinessPage from "../presentation/page/BusinessPage";

const breadCrumbs = [
    "Dashboard"
]

interface IBusinessPage{
    business: Array<IBusiness>
}

const Business = ({business} : IBusinessPage) => {
  return (
      <Layout breadCrumbs={breadCrumbs}>
        <BusinessPage business={business} />
      </Layout>
  )
}

export async function getServerSideProps({ locale, req }: any) {
    const { cookies } = req;

    if (
        cookies[USER_FIRST_PART] === undefined &&
        cookies[USER_SECOND_PART] === undefined &&
        cookies[USER_THIRD_PART] === undefined
    ) {
        return {
            redirect: {
                destination: '/login',
                permanent: false,
            },
        }
    }
    const response = await businessService.getBusinessApi(cookies[USER_FIRST_PART] +
        '.' +
        cookies[USER_SECOND_PART] +
        '.' +
        cookies[USER_THIRD_PART]
    );
    return{
        props: {
            business: response.data !== undefined ? response.data : []
        }
    }

}

export default Business
