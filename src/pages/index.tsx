import Layout from "presentation/layout/Layout";
import HomePage from "presentation/page/HomePage";
import {USER_FIRST_PART, USER_SECOND_PART, USER_THIRD_PART} from "data/static/storageNames";
import {IUser} from "domain/interfaces/IUser";
import {userService} from "data/API";

const breadCrumbs = [
    "Dashboard"
]

interface IHome{
    users: Array<IUser>
}

const Home = ({users} : IHome) => {
  return (
      <Layout breadCrumbs={breadCrumbs}>
        <HomePage users={users} />
      </Layout>
  )
}

export async function getServerSideProps({ locale, req }: any) {
    const { cookies } = req;

    if (
        cookies[USER_FIRST_PART] === undefined &&
        cookies[USER_SECOND_PART] === undefined &&
        cookies[USER_THIRD_PART] === undefined
    ) {
        return {
            redirect: {
                destination: '/login',
                permanent: false,
            },
        }
    }
    const response = await userService.getUsersApi(cookies[USER_FIRST_PART] +
        '.' +
        cookies[USER_SECOND_PART] +
        '.' +
        cookies[USER_THIRD_PART]
    );
    return{
        props: {
            users: response.data !== undefined ? response.data : []
        }
    }

}

export default Home
