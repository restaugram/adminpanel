import {USER_FIRST_PART, USER_ROLE, USER_SECOND_PART, USER_THIRD_PART} from "data/static/storageNames";
import {adminsService} from "data/API";

import Layout from "presentation/layout/Layout";
import AdminsPage from "presentation/page/AdminsPage";
import {IAdmin} from "domain/interfaces/IAdmin";

const breadCrumbs = [
    "Dashboard"
]

interface IAdmins{
    admins: Array<IAdmin>
}

const Admins = ({admins} : IAdmins) => {
    return (
        <Layout breadCrumbs={breadCrumbs}>
            <AdminsPage admins={admins} />
        </Layout>
    )
}

export async function getServerSideProps({ locale, req }: any) {
    const { cookies } = req;
    if (
        cookies[USER_FIRST_PART] === undefined &&
        cookies[USER_SECOND_PART] === undefined &&
        cookies[USER_THIRD_PART] === undefined && cookies[USER_ROLE] !== 'SuperAdmin'
    ) {
        return {
            redirect: {
                destination: '/login',
                permanent: false,
            },
        }
    }

    const response = await adminsService.getAdminsApi(cookies[USER_FIRST_PART] +
        '.' +
        cookies[USER_SECOND_PART] +
        '.' +
        cookies[USER_THIRD_PART]
    );
    return{
        props: {
            admins: response.data !== undefined ? response.data : []
        }
    }

}

export default Admins
